package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		//TODO: Complete this method
		return "" + this.getValue();
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature temp = new Celsius( (float)(this.getValue()*1.8 + 32));
		return temp;
	}
}
