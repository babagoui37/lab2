package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature{
	public Fahrenheit(float t)
	{
		super(t);
		
	}
	
	public String toString() 
	{
		//TODO: Complete this method
		return "" + this.getValue();
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature temp = new Fahrenheit( (float) ((this.getValue() - 32) / 1.8000));
		return temp;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}

}
